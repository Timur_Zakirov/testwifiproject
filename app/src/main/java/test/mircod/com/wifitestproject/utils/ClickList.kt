package test.mircod.com.wifitestproject.utils

import android.net.nsd.NsdServiceInfo

interface ClickList {
    fun onClick(nsdServiceInfo: NsdServiceInfo)
}
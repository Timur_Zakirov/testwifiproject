package test.mircod.com.wifitestproject

import android.Manifest
import android.app.Activity
import android.app.Application
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.disposables.CompositeDisposable
import test.mircod.com.wifitestproject.utils.TimberTree
import timber.log.Timber

class WifiApp : Application() {

    override fun onCreate() {
        super.onCreate()
        Timber.plant(TimberTree())
    }
}
package test.mircod.com.wifitestproject.wifi_nsd

import android.content.Context
import android.net.nsd.NsdManager
import android.net.nsd.NsdServiceInfo
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import android.content.Context.WIFI_SERVICE
import android.support.v4.content.ContextCompat.getSystemService
import android.net.wifi.WifiManager
import android.text.format.Formatter
import android.widget.Toast
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.json.JSONObject
import test.mircod.com.wifitestproject.MainActivity
import java.io.DataInputStream
import java.io.DataOutputStream
import java.io.IOException
import java.net.Socket


const val SERVICE_TYPE = "_http._tcp."
const val REQUEST_CONNECT_CLIENT = "request-connect-client"
const val SocketServerPort = 6000

class WifiNsd(val context: Context) {
    private var mServiceInfo: NsdServiceInfo? = null
    private var nsdManager: NsdManager? = null
    var SERVICE_NAME = android.os.Build.BRAND + android.os.Build.MODEL


    fun registerService(port: Int) {
        val serviceInfo = NsdServiceInfo().also {
            it.serviceName = SERVICE_NAME
            it.serviceType = SERVICE_TYPE
            it.port = port
        }
        nsdManager = (context.getSystemService(Context.NSD_SERVICE) as NsdManager)
                .apply {
                    registerService(serviceInfo, NsdManager.PROTOCOL_DNS_SD, mRegistrationListener)
                }
    }


    private val mRegistrationListener = object : NsdManager.RegistrationListener {
        override fun onUnregistrationFailed(serviceInfo: NsdServiceInfo?, errorCode: Int) {
            Timber.e("Unregistration failed: $errorCode serviceInfo: $serviceInfo")
        }

        override fun onServiceUnregistered(serviceInfo: NsdServiceInfo?) {
            Timber.e("Service unregistered: $serviceInfo")
        }

        override fun onRegistrationFailed(serviceInfo: NsdServiceInfo?, errorCode: Int) {
            Timber.e("Registration failed: $errorCode serviceInfo: $serviceInfo")
        }

        override fun onServiceRegistered(serviceInfo: NsdServiceInfo?) {
            mServiceInfo = serviceInfo
            nsdManager?.discoverServices(SERVICE_TYPE, NsdManager.PROTOCOL_DNS_SD, discoveryListner)
            Timber.e("service registered serviceInfo: $serviceInfo")
        }

    }

    val discoverServices = PublishSubject.create<NsdServiceInfo>()
    val resolveServices = PublishSubject.create<NsdServiceInfo>()

    fun getDiscoverServiceSubject(): Observable<NsdServiceInfo> = discoverServices.hide()
    fun getResolveServiceSubject(): Observable<NsdServiceInfo> = resolveServices.hide()

    private val discoveryListner = object : NsdManager.DiscoveryListener {
        override fun onServiceFound(serviceInfo: NsdServiceInfo) {
            Timber.e("serviceInfo: $serviceInfo")
            discoverServices.onNext(serviceInfo)
            when {
                serviceInfo.serviceType != SERVICE_TYPE -> // Service type is the string containing the protocol and
                    // transport layer for this service.
                    Timber.d("Unknown Service Type: ${serviceInfo.serviceType}")
                serviceInfo.serviceName == SERVICE_NAME -> // The name of the service tells the user what they'd be
                    // connecting to. It could be "Bob's Chat App".
                    Timber.d("Same machine: $SERVICE_NAME")
                else -> {
                    Timber.d("serviceInfo: $serviceInfo")
                }
            }
        }

        override fun onStopDiscoveryFailed(serviceType: String?, errorCode: Int) {
            Timber.e("SERVICE_TYPE: $serviceType errorCode: $errorCode")
            nsdManager?.stopServiceDiscovery(this)
        }

        override fun onStartDiscoveryFailed(serviceType: String?, errorCode: Int) {
            Timber.e("SERVICE_TYPE: $serviceType errorCode: $errorCode")
            nsdManager?.stopServiceDiscovery(this)
        }

        override fun onDiscoveryStarted(serviceType: String?) {
            Timber.e("SERVICE_TYPE: $serviceType ")
        }

        override fun onDiscoveryStopped(serviceType: String?) {
            Timber.e("SERVICE_TYPE: $serviceType ")
        }

        override fun onServiceLost(serviceInfo: NsdServiceInfo?) {
            Timber.e("serviceInfo: $serviceInfo ")
        }
    }

    fun resolve(nsdServiceInfo: NsdServiceInfo) {
        nsdManager?.resolveService(nsdServiceInfo, mResolveListner)
    }

    fun tearDown() {
        nsdManager?.apply {
            stopServiceDiscovery(discoveryListner)
            unregisterService(mRegistrationListener)
        }
    }

    private val mResolveListner = object : NsdManager.ResolveListener {
        override fun onServiceResolved(serviceInfo: NsdServiceInfo) {
            Timber.e("onServiceResolved: $serviceInfo")
            resolveServices.onNext(serviceInfo!!)
            connect(serviceInfo)
        }

        override fun onResolveFailed(serviceInfo: NsdServiceInfo?, errorCode: Int) {
            Timber.e("onResolveFailed: $serviceInfo errorCode: $errorCode")
        }

    }

    private fun connect(serviceInfo: NsdServiceInfo) {
        if (serviceInfo.serviceName == SERVICE_NAME) {
            Timber.e("Same IP.")
            return
        }

        val ipAddress = getLocationIpAddress()
        val jsonData = JSONObject()

        jsonData.put("request", REQUEST_CONNECT_CLIENT)
        jsonData.put("device", SERVICE_NAME)
        jsonData.put("ipAddress", ipAddress)
        createSocket(jsonData, serviceInfo)
    }

    private fun getLocationIpAddress(): String {
        val wm = context.applicationContext.getSystemService(WIFI_SERVICE) as WifiManager?
        return Formatter.formatIpAddress(wm!!.connectionInfo.ipAddress)
    }

    private fun createSocket(jsonObject: JSONObject, serviceInfo: NsdServiceInfo) {
        var isConnect = false
        val subscribe = Observable.just(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe({
                    var socket: Socket? = null
                    var dataOutStream: DataOutputStream? = null
                    var dataInputStream: DataInputStream? = null
                    try {
                        socket = Socket(serviceInfo.host, SocketServerPort)
                        dataOutStream = DataOutputStream(socket.getOutputStream())
                        dataInputStream = DataInputStream(socket.getInputStream())

                        dataOutStream.writeUTF(jsonObject.toString())
                        Timber.d("waiting response")

                        val respnose = dataInputStream.readUTF()
                        Timber.d("respnose: $respnose")
                        (context as MainActivity).runOnUiThread {
                            Toast.makeText(context, respnose, Toast.LENGTH_SHORT).show()
                        }
                        isConnect = (respnose != null && respnose == "Connection Accepted")
                    } catch (exception: IOException) {
                        Timber.e("error: $exception")
                    } finally {
                        socket?.close()
                        dataInputStream?.close()
                        dataOutStream?.close()
                    }
                }, {
                    Timber.e("Error: $it")
                })

    }
}
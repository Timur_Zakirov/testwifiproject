package test.mircod.com.wifitestproject.scan_wifi

import android.databinding.DataBindingUtil
import android.net.wifi.ScanResult
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import test.mircod.com.wifitestproject.R
import test.mircod.com.wifitestproject.databinding.ItemWifiResultBinding

class WifiScanAdapter : RecyclerView.Adapter<WifiScanAdapter.WifiViewHolder>() {
    var wifiScanResults = ArrayList<ScanResult>()

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): WifiViewHolder {
        val dataBind: ItemWifiResultBinding =
            DataBindingUtil.inflate(LayoutInflater.from(p0.context),
                R.layout.item_wifi_result, p0, false)
        return WifiViewHolder(dataBind)
    }

    override fun getItemCount(): Int = wifiScanResults.size

    override fun onBindViewHolder(p0: WifiViewHolder, p1: Int) {
        p0.bind(wifiScanResults[p1])
    }

    fun addScanResults(scanResult: List<ScanResult>) {
        wifiScanResults.clear()
        wifiScanResults.addAll(scanResult)
        notifyDataSetChanged()
    }



    inner class WifiViewHolder(val dataBind: ItemWifiResultBinding) : RecyclerView.ViewHolder(dataBind.root) {
        fun bind(scanResult: ScanResult) {
            dataBind.scanResult = scanResult
            dataBind.notifyChange()
        }
    }
}
package test.mircod.com.wifitestproject.wifi_nsd

import android.databinding.DataBindingUtil
import android.net.nsd.NsdServiceInfo
import android.net.wifi.ScanResult
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import test.mircod.com.wifitestproject.R
import test.mircod.com.wifitestproject.databinding.ItemNsdInfoBinding
import test.mircod.com.wifitestproject.utils.ClickList

class NsdAdapter(val listener: ClickList) : RecyclerView.Adapter<NsdAdapter.NsdViewHolder>() {

    var wifiScanResults = ArrayList<NsdServiceInfo>()

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): NsdViewHolder {
        val dataBind: ItemNsdInfoBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(p0.context),
                R.layout.item_nsd_info, p0, false
            )
        return NsdViewHolder(dataBind)
    }

    override fun getItemCount(): Int = wifiScanResults.size

    override fun onBindViewHolder(p0: NsdViewHolder, p1: Int) {
        p0.bind(wifiScanResults[p1])
    }

    fun addScanResults(scanResult: NsdServiceInfo) {
        if (wifiScanResults.contains(scanResult)) return
        if (scanResult.serviceName == (android.os.Build.BRAND + android.os.Build.MODEL)) return
        wifiScanResults.add(scanResult)
        notifyItemInserted(wifiScanResults.size - 1)
    }

    fun updateItem(scanResult: NsdServiceInfo) {
        wifiScanResults.map {
            if (it.serviceName == scanResult.serviceName) {
                it.host = scanResult.host
                it.port = scanResult.port
                notifyItemChanged(wifiScanResults.indexOf(it))
            }
        }
    }

    inner class NsdViewHolder(val dataBind: ItemNsdInfoBinding) : RecyclerView.ViewHolder(dataBind.root) {
        fun bind(nsdServiceInfo: NsdServiceInfo) {
            dataBind.nsdInfo = nsdServiceInfo
            dataBind.notifyChange()
            dataBind.listener = listener
        }
    }
}
package test.mircod.com.wifitestproject

import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.content.Intent
import android.net.nsd.NsdServiceInfo
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber
import test.mircod.com.wifitestproject.utils.TimberTree
import test.mircod.com.wifitestproject.wifi_nsd.WifiNsd
import test.mircod.com.wifitestproject.scan_wifi.WifiScan
import test.mircod.com.wifitestproject.scan_wifi.WifiScanAdapter
import test.mircod.com.wifitestproject.utils.ClickList
import test.mircod.com.wifitestproject.wifi_nsd.NsdAdapter
import test.mircod.com.wifitestproject.wifi_nsd.ServerActivity

class MainActivity : AppCompatActivity(), View.OnClickListener, ClickList {


    private val wifiNsd = WifiNsd(this)
    private val nsdAdapter = NsdAdapter(this)
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
    private val random = (1000..9999).random()
    override fun onClick(nsdServiceInfo: NsdServiceInfo) {
        wifiNsd.resolve(nsdServiceInfo)
        compositeDisposable.add(wifiNsd.getResolveServiceSubject()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                nsdAdapter.updateItem(it)
            })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val rxPermissions = RxPermissions(this)
        compositeDisposable.add(rxPermissions.request(ACCESS_COARSE_LOCATION, ACCESS_FINE_LOCATION)
            .subscribe {
                Timber.d("its ok Permission")
            })
    }

    override fun onResume() {
        super.onResume()
        wifiNsd.apply {
            Timber.e("port: $random")
            registerService(random)
            wifiNsd.getDiscoverServiceSubject()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    nsdAdapter.addScanResults(it)
                }
            rvWifi.layoutManager = LinearLayoutManager(this@MainActivity)
            rvWifi.adapter = nsdAdapter
        }
    }

    override fun onPause() {
        wifiNsd.tearDown()
        compositeDisposable.dispose()
        super.onPause()
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnStartScan -> {
                val wifiAdapter = WifiScanAdapter()
                rvWifi.layoutManager = LinearLayoutManager(this)
                rvWifi.adapter = wifiAdapter


                val wifiCore = WifiScan(this)
                wifiCore.startScanWifi()
                    .subscribe {
                        wifiAdapter.addScanResults(it)
                    }
            }
            R.id.btnStartNsd -> {
                val intent = Intent(this, ServerActivity::class.java)
                startActivity(intent)
            }
        }
    }
}

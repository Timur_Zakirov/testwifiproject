package test.mircod.com.wifitestproject.wifi_nsd

import android.os.Bundle
import org.json.JSONException
import org.json.JSONObject
import android.app.Activity
import android.widget.Toast
import test.mircod.com.wifitestproject.R
import timber.log.Timber
import java.io.DataInputStream
import java.io.DataOutputStream
import java.io.IOException
import java.net.ServerSocket
import java.net.Socket
import kotlinx.android.synthetic.main.activity_server_nsd.*


class ServerActivity : Activity() {


    private var clientIPs: MutableList<String>? = null
    // NSD Manager and service registration code

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_server_nsd)
        clientIPs = ArrayList()
        // NSD stuff.
        val socketServerThread = SocketServerThread()
        socketServerThread.start()
    }

    private inner class SocketServerThread : Thread() {

        override fun run() {
            var socket: Socket? = null
            var dataInputStream: DataInputStream? = null
            var dataOutputStream: DataOutputStream? = null

            try {
                Timber.i("Creating server socket")
                val serverSocket = ServerSocket(SocketServerPort)
                while (true) {
                    socket = serverSocket.accept()
                    dataInputStream = DataInputStream(socket!!.getInputStream())
                    dataOutputStream = DataOutputStream(socket.getOutputStream())
                    val messageFromClient: String = dataInputStream.readUTF()
                    val messageToClient: String
                    val request: String

                    //If no message sent from client, this code will block the program
                    val jsondata = JSONObject(messageFromClient)

                    try {
                        request = jsondata.getString("request")
                        if (request == REQUEST_CONNECT_CLIENT) {
                            val clientIPAddress = jsondata.getString("ipAddress")
                            val device = jsondata.getString("device")

                            // Add client IP to a list
                            clientIPs!!.add(clientIPAddress)
                            runOnUiThread {
                                tvDevices.text = "${tvDevices.text}  \n  $clientIPAddress $device"

                                Toast.makeText(
                                        this@ServerActivity, "Connected via: $clientIPAddress", Toast.LENGTH_SHORT
                                ).show()
                            }
                            messageToClient = "Connection Accepted"
                            dataOutputStream.writeUTF(messageToClient)
                        } else {
                            // There might be other queries, but as of now nothing.
                            dataOutputStream.flush()
                        }
                    } catch (e: JSONException) {
                        Timber.e("Unable to get request")
                        runOnUiThread {
                            Toast.makeText(this@ServerActivity, "Unable to get request", Toast.LENGTH_SHORT).show()
                        }
                        dataOutputStream.flush()
                    }
                }
            } catch (e: IOException) {
                Timber.e("exception: $e")
            } finally {
                socket?.close()
                dataInputStream?.close()
                dataOutputStream?.close()
            }
        }
    }
}

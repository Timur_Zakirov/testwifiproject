package test.mircod.com.wifitestproject.scan_wifi

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.nsd.NsdServiceInfo
import android.net.wifi.ScanResult
import android.net.wifi.WifiManager
import android.widget.Toast
import io.reactivex.Observable
import timber.log.Timber

class WifiScan(val context: Context) {
    lateinit var wifiManager: WifiManager

    init {
        if (checkWifiConnection()) {
            Timber.d("Connect successful")
        } else {
            Toast.makeText(context, "Wifi don`t connect", Toast.LENGTH_SHORT).show()
        }
    }

    private fun checkWifiConnection(): Boolean {
        val connManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as (ConnectivityManager)
        val wifiInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
        val networkCapabilities = connManager.getNetworkCapabilities(connManager.activeNetwork)
        return networkCapabilities != null && networkCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_VALIDATED)
    }

    fun startScanWifi(): Observable<List<ScanResult>> {
        return if (checkWifiConnection()) {
            Timber.d("start intent filter")
            wifiManager = context.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
            val intentFilter = IntentFilter()
            intentFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)
            context.registerReceiver(wifiScanReceiver, intentFilter)
            scanResponse(true)
            Observable.just(wifiManager.scanResults.toList())
        } else {
            Observable.just(null)
        }
    }

    val wifiScanReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            Timber.d("OnReceive ")
            val isScanSuccess = intent.getBooleanExtra(WifiManager.EXTRA_RESULTS_UPDATED, false)
            scanResponse(isScanSuccess)
        }
    }

    fun scanResponse(isScanSuccess: Boolean) {
        if (isScanSuccess) {
            wifiManager.scanResults.forEach {
                Timber.e("wifi Scan Result: $it")
            }

        } else {
            Toast.makeText(context, "Wifi scan failed!", Toast.LENGTH_SHORT).show()
        }
    }

    fun registerService(port: Int) {
        val serviceInfo = NsdServiceInfo().also {
            it.serviceName = "TestWifi"
            it.serviceType = "_testwifi._tcp"
        }
    }


}
package test.mircod.com.wifitestproject.nearby

import android.app.Activity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.nearby.Nearby
import com.google.android.gms.nearby.connection.*
import test.mircod.com.wifitestproject.R
import timber.log.Timber
import com.google.android.gms.nearby.connection.DiscoveryOptions
import com.google.android.gms.nearby.connection.DiscoveredEndpointInfo
import com.google.android.gms.nearby.connection.EndpointDiscoveryCallback
import test.mircod.com.wifitestproject.utils.TimberTree


private const val SERVICE_ID = "Mircod A"

class NearbyActivity : Activity(), View.OnClickListener, GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener {
    override fun onConnectionFailed(p0: ConnectionResult) {
        Timber.e("failed: ${p0.errorMessage}")
    }

    override fun onConnected(p0: Bundle?) {
        Timber.e("onConnected: $p0")
    }

    override fun onConnectionSuspended(p0: Int) {
        Timber.e("onConnectionSuspended: $p0")
    }

    var googleApiClient: GoogleApiClient? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.nearby_activity)
        Timber.plant(TimberTree())

        googleApiClient = GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(Nearby.CONNECTIONS_API)
            .build()
    }

    public override fun onStart() {
        super.onStart()
        Timber.d("onStart")
        googleApiClient!!.connect()
    }

    public override fun onStop() {
        super.onStop()
        Timber.d("onStop")
        if (googleApiClient != null) {
            googleApiClient!!.disconnect()
        }
    }


    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnStartScanNearby -> {
                Timber.e("btnStartScanNearby")
                startAdvertising()
            }
            R.id.btnDiscover -> {
                Timber.e("btnDiscover")
                discover()
            }
        }
    }

    private val mEndpointDiscoveryCallback = object : EndpointDiscoveryCallback() {
        override fun onEndpointFound(endpointId: String, discoveredEndpointInfo: DiscoveredEndpointInfo) {
            Timber.e("Endpoint found with id: $endpointId")
        }

        override fun onEndpointLost(endpointId: String) {
            Timber.e("Endpoint lost with id: $endpointId")
        }
    }

    private fun discover() {
        Nearby.getConnectionsClient(applicationContext).startDiscovery(
            SERVICE_ID,
            mEndpointDiscoveryCallback,
            DiscoveryOptions(Strategy.P2P_STAR)
        )
            .addOnSuccessListener {
                Timber.e("Discover success")
            }
            .addOnFailureListener {
                Timber.e("Discover fails with: $it")
            }
    }

    private fun startAdvertising() {
        Nearby.getConnectionsClient(this)
            .startAdvertising(
                SERVICE_ID,
                "test.mircod.com.wifitestproject.nearby",
                mConnectionLifecycleCallback,
                AdvertisingOptions(Strategy.P2P_STAR)
            ).addOnSuccessListener {
                Timber.e("ON Success")
            }.addOnFailureListener {
                Timber.e("ON Failure ${it.cause}")
            }
    }

    private val mConnectionLifecycleCallback = object : ConnectionLifecycleCallback() {

        override fun onConnectionInitiated(endpointId: String, connectionInfo: ConnectionInfo) {
            // Automatically accept the connection on both sides.
            Timber.e("endpoint:$endpointId connectionInfo: $connectionInfo")
            Nearby.getConnectionsClient(applicationContext).acceptConnection(endpointId, payloadCallback)
        }

        override fun onConnectionResult(endpointId: String, result: ConnectionResolution) {
            Timber.e("endpoint:$endpointId result: $result")
            when (result.status.statusCode) {
                ConnectionsStatusCodes.STATUS_OK -> {
                }
                ConnectionsStatusCodes.STATUS_CONNECTION_REJECTED -> {
                }
                ConnectionsStatusCodes.STATUS_ERROR -> {
                }
            }// We're connected! Can now start sending and receiving data.
            // The connection was rejected by one or both sides.
            // The connection broke before it was able to be accepted.
        }

        override fun onDisconnected(endpointId: String) {
            Timber.e("Disconnected: $endpointId")
            // We've been disconnected from this endpoint. No more data can be
            // sent or received.
        }
    }

    val payloadCallback = object : PayloadCallback() {
        override fun onPayloadReceived(endpointID: String, data: Payload) {
            Timber.e("endpointID: $endpointID data: $data")
        }

        override fun onPayloadTransferUpdate(endpointID: String, dataUpdate: PayloadTransferUpdate) {
            Timber.e("endpointID: $endpointID dataUpdate: $dataUpdate")
        }

    }
}
package mircod.com.wifi_nds_sdk.core

interface IWifiCore {
    fun register()
    fun discover()
    fun resolution()
    fun connect()
    fun startServer()
}